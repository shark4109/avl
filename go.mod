module gitlab.com/shark4109/avl

go 1.17

require (
	github.com/npat-efault/crc16 v0.0.0-20161013170008-4128ccbe47c3
	github.com/rs/zerolog v1.24.0
)
