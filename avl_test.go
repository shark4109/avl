package avl

import (
	"encoding/hex"
	"reflect"
	"testing"
	"time"
)

type TestCaseAVLDataPacket struct {
	Input  string
	Output *AVLDataPacket
}

var testCases = []TestCaseAVLDataPacket{
	{
		Input: "000000000000003608010000016B40D8EA30010000000000000000000000000000000105021503010101425E0F01F10000601A014E0000000000000000010000C7CF",
		Output: &AVLDataPacket{
			DataFieldLength: 54,
			CodecID:         8,
			NumberOfData1:   1,
			AVLData: []AVLData{
				{
					Timestamp: time.UnixMilli(1560161086000),
					Priority:  1,
					IOElement: IOElement{
						EventID: 1,
						N1: []Element{
							{
								ID:    21,
								Value: []byte{0x03},
							},
							{
								ID:    1,
								Value: []byte{0x01},
							},
						},
						N2: []Element{
							{
								ID:    66,
								Value: []byte{0x5E, 0x0F},
							},
						},
						N4: []Element{
							{
								ID:    241,
								Value: []byte{0x00, 0x00, 0x60, 0x1A},
							},
						},
						N8: []Element{
							{
								ID:    78,
								Value: []byte{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
							},
						},
					},
				},
			},
			NumberOfData2:   1,
			CRC16:           51151,
			CalculatedCRC16: 51151,
		},
	},
	{
		Input: "000000000000002808010000016B40D9AD80010000000000000000000000000000000103021503010101425E100000010000F22A",
		Output: &AVLDataPacket{
			DataFieldLength: 40,
			CodecID:         8,
			NumberOfData1:   1,
			AVLData: []AVLData{
				{
					Timestamp: time.UnixMilli(1560161136000),
					Priority:  1,
					IOElement: IOElement{
						EventID: 1,
						N1: []Element{
							{
								ID:    21,
								Value: []byte{0x03},
							},
							{
								ID:    1,
								Value: []byte{0x01},
							},
						},
						N2: []Element{
							{
								ID:    66,
								Value: []byte{0x5E, 0x10},
							},
						},
					},
				},
			},
			NumberOfData2:   1,
			CRC16:           61994,
			CalculatedCRC16: 61994,
		},
	},
	{
		Input: "000000000000004308020000016B40D57B480100000000000000000000000000000001010101000000000000016B40D5C198010000000000000000000000000000000101010101000000020000252C",
		Output: &AVLDataPacket{
			DataFieldLength: 67,
			CodecID:         8,
			NumberOfData1:   2,
			AVLData: []AVLData{
				{
					Timestamp: time.UnixMilli(1560160861000),
					Priority:  0x01,
					IOElement: IOElement{
						EventID: 1,
						N1: []Element{
							{
								ID:    1,
								Value: []byte{0x00},
							},
						},
					},
				},
				{
					Timestamp: time.UnixMilli(1560160879000),
					Priority:  1,
					IOElement: IOElement{
						EventID: 1,
						N1: []Element{
							{
								ID:    1,
								Value: []byte{0x01},
							},
						},
					},
				},
			},
			NumberOfData2:   0x02,
			CRC16:           9516,
			CalculatedCRC16: 9516,
		},
	},
	{
		Input: "000000000000004A8E010000016B412CEE000100000000000000000000000000000000010005000100010100010011001D00010010015E2C880002000B000000003544C87A000E000000001DD7E06A00000100002994",
		Output: &AVLDataPacket{
			DataFieldLength: 74,
			CodecID:         0x8E,
			NumberOfData1:   1,
			AVLData: []AVLData{
				{
					Timestamp: time.UnixMilli(0x0000016B412CEE00),
					Priority:  0x01,
					IOElement: IOElement{
						EventID: 0x0001,
						N1: []Element{
							{
								ID:    0x0001,
								Value: []byte{0x01},
							},
						},
						N2: []Element{
							{
								ID:    0x0011,
								Value: []byte{0x00, 0x1D},
							},
						},
						N4: []Element{
							{
								ID:    0x0010,
								Value: []byte{0x01, 0x5E, 0x2C, 0x88},
							},
						},
						N8: []Element{
							{
								ID:    0x000B,
								Value: []byte{0x00, 0x00, 0x00, 0x00, 0x35, 0x44, 0xC8, 0x7A},
							},
							{
								ID:    0x000E,
								Value: []byte{0x00, 0x00, 0x00, 0x00, 0x1D, 0xD7, 0xE0, 0x6A},
							},
						},
						// NX: []Element{},
					},
				},
			},
			NumberOfData2:   1,
			CRC16:           0x00002994,
			CalculatedCRC16: 0x00002994,
		},
	},
}

func TestDecodeAVLDataPacket(t *testing.T) {
	for _, testCase := range testCases {
		testData, _ := hex.DecodeString(testCase.Input)
		avlDataPacket, _ := DecodeAVLDataPacket(testData)
		if !reflect.DeepEqual(avlDataPacket, testCase.Output) {
			t.Errorf("AVLPacketData \ngot:\t%v\nwant:\t%v", avlDataPacket, testCase.Output)
		}
	}
}

func TestDecodeAVLData(t *testing.T) {
	testAVLData := []AVLData{
		{
			Timestamp: time.UnixMilli(1560161086000),
			Priority:  1,
			IOElement: IOElement{
				EventID: 1,
				N1: []Element{
					{
						ID:     21,
						Length: 0,
						Value:  []byte{0x03},
					},
					{
						ID:     1,
						Length: 0,
						Value:  []byte{0x01},
					},
				},
				N2: []Element{
					{
						ID:     66,
						Length: 0,
						Value:  []byte{0x5E, 0x0F},
					},
				},
				N4: []Element{
					{
						ID:     241,
						Length: 0,
						Value:  []byte{0x00, 0x00, 0x60, 0x1A},
					},
				},
				N8: []Element{
					{
						ID:     78,
						Length: 0,
						Value:  []byte{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
					},
				},
			},
		},
	}
	testData, _ := hex.DecodeString("0000016B40D8EA30010000000000000000000000000000000105021503010101425E0F01F10000601A014E0000000000000000")
	avlDataPacket := &AVLDataPacket{
		NumberOfData1: 1,
		CodecID:       8,
		NumberOfData2: 1,
	}
	DecodeAVLData(testData, avlDataPacket)
	if !reflect.DeepEqual(avlDataPacket.AVLData, testAVLData) {
		t.Errorf("AVLData \ngot:\t%v\nwant:\t%v", avlDataPacket.AVLData, testAVLData)
	}
}

func TestDecodeGPSElement(t *testing.T) {
	testAVLData := AVLData{
		GPSElement: GPSElement{
			Longitude:  54.714638,
			Latitude:   0,
			Altitude:   0,
			Angle:      0,
			Satellites: 0,
			Speed:      0,
		},
	}
	testData, _ := hex.DecodeString("209CCA800000000000000000000000")
	avlData := AVLData{}
	DecodeGPSElement(testData, &avlData)
	if !reflect.DeepEqual(avlData, testAVLData) {
		t.Errorf("AVLData \ngot:\t%v\nwant:\t%v", avlData, testAVLData)
	}
}
