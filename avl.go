package avl

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"reflect"
	"strconv"
	"time"

	"github.com/npat-efault/crc16"
	"github.com/rs/zerolog/log"
)

type AVLDataPacket struct {
	Preamble        uint32    `bson:"preamble"`
	DataFieldLength uint32    `bson:"data_filed_length"`
	CodecID         uint8     `bson:"codec_id"`
	NumberOfData1   uint8     `bson:"number_of_data_1"` // DataLength1 should be the same as DataLength2
	AVLData         []AVLData `bson:"avl_data"`
	NumberOfData2   uint8     `bson:"number_of_data_2"` // DataLength2 should be the same as DataLength1
	CRC16           uint16    `bson:"crc16"`
	CalculatedCRC16 uint16    `bson:"calculated_crc16"`
}

func (avlDataPacket *AVLDataPacket) Valid() bool {
	return avlDataPacket.NumberOfData1 == avlDataPacket.NumberOfData2 &&
		avlDataPacket.CRC16 == avlDataPacket.CalculatedCRC16
}

type AVLData struct {
	Timestamp time.Time `bson:"timestamp"`
	// Priority can have one of the following values
	// 0 - Low
	// 1 - High
	// 2 - Panic
	Priority   uint8      `bson:"priority"`
	GPSElement GPSElement `bson:"gps_element"`
	IOElement  IOElement  `bson:"io_element"`
}

type IOElement struct {
	EventID uint16
	N1      []Element `bson:"n1"`
	N2      []Element `bson:"n2"`
	N4      []Element `bson:"n4"`
	N8      []Element `bson:"n8"`
	NX      []Element `bson:"nx"`
}

type Element struct {
	ID     uint16 `bson:"id"`
	Length uint16 `bson:"length"`
	Value  []byte `bson:"value"`
}

type GPSElement struct {
	Longitude  float32 `bson:"longitude"`
	Latitude   float32 `bson:"latitude"`
	Altitude   uint16  `bson:"altitude"`
	Angle      uint16  `bson:"angle"`
	Satellites uint8   `bson:"satellites"`
	Speed      uint16  `bson:"speed"`
}

func (gpsElement *GPSElement) Valid() bool {
	return gpsElement.Speed != 0
}

func DecodeAVLDataPacket(data []byte) (*AVLDataPacket, error) {
	var n uint64
	var err error
	var avlDataBytes []byte
	tmp := make([]byte, 4)
	bytesReader := bytes.NewReader(data)
	avlDataPacket := &AVLDataPacket{}

	// read first 4 bytes which should be 00000000
	bytesReader.Read(tmp)
	if !reflect.DeepEqual([]byte{0x00, 0x00, 0x00, 0x00}, tmp) {
		log.Error().Msg("preamble is not correct")
		return nil, err
	}
	// read data field length in next 4 bytes
	bytesReader.Read(tmp)
	n, err = strconv.ParseUint(hex.EncodeToString(tmp), 16, 32)
	if err != nil {
		log.Fatal().Msg("could not decode length")
	}
	avlDataPacket.DataFieldLength = uint32(n)
	// Read All Data
	tmp = make([]byte, avlDataPacket.DataFieldLength)
	bytesReader.Read(tmp)
	crc := crc16.Checksum(&crc16.Conf{
		Poly: 0x8005, BitRev: true,
		IniVal: 0x0, FinVal: 0x0,
		BigEnd: false,
	}, tmp)
	avlDataPacket.CalculatedCRC16 = crc
	// read codec ID
	n, err = strconv.ParseUint(hex.EncodeToString(tmp[:1]), 16, 32)
	if err != nil {
		log.Error().Msg("could not get codec ID")
	}
	avlDataPacket.CodecID = uint8(n)
	// read how many records is in the packet
	n, err = strconv.ParseUint(hex.EncodeToString(tmp[1:2]), 16, 32)
	if err != nil {
		log.Error().Msg("could not decode number of packets")
	}
	avlDataPacket.NumberOfData1 = uint8(n)
	// read how many records is in the packed (should be same as before)
	n, err = strconv.ParseUint(hex.EncodeToString(tmp[len(tmp)-1:]), 16, 32)
	if err != nil {
		log.Error().Msg("could not decode number of packets")
	}
	avlDataPacket.NumberOfData2 = uint8(n)

	// Create copy from tmp AVLData
	avlDataBytes = make([]byte, len(tmp[2:len(tmp)-1]))
	copy(avlDataBytes, tmp[2:len(tmp)-1])

	// read CRC field length in next 4 bytes
	tmp = make([]byte, 4)
	bytesReader.Read(tmp)
	n, err = strconv.ParseUint(hex.EncodeToString(tmp), 16, 32)
	if err != nil {
		log.Error().Msg("could not get CRC16")
		return nil, err
	}
	avlDataPacket.CRC16 = uint16(n)

	// Check if calculated CRC equal to provided CRC
	if !avlDataPacket.Valid() {
		log.Debug().Interface("avlDataPacket", avlDataPacket).Send()
		log.Error().Msg("avlDataPacket is not valid")
		return nil, errors.New("invalid avlDataPacket")
	}

	// read actual AVLData (calculate for bytes that are used for DataLength1 and DataLength2)
	err = DecodeAVLData(avlDataBytes, avlDataPacket)
	if err != nil {
		return nil, err
	}

	return avlDataPacket, err
}

func bytes2int(data []byte) uint64 {
	var n uint64
	for _, b := range data {
		n = (n << 8) | uint64(b)
	}
	return n
}

func DecodeAVLData(data []byte, avlDataPacket *AVLDataPacket) error {
	var err error
	var tmp []byte
	avlDataPacket.AVLData = make([]AVLData, avlDataPacket.NumberOfData1)
	bytesReader := bytes.NewReader(data)

	for index := 0; index < len(avlDataPacket.AVLData); index++ {
		avl := &AVLData{}

		// read timestamp
		tmp = make([]byte, 8)
		bytesReader.Read(tmp)
		avl.Timestamp = time.UnixMilli(int64(binary.BigEndian.Uint64(tmp)))

		// read priority
		tmp = make([]byte, 1)
		bytesReader.Read(tmp)

		avl.Priority = uint8(bytes2int(tmp))

		// read location
		tmp = make([]byte, 15)
		bytesReader.Read(tmp)
		DecodeGPSElement(tmp, avl)

		// read IOElement EventID
		if avlDataPacket.CodecID == 8 {
			tmp = make([]byte, 1)
		} else if avlDataPacket.CodecID == 142 {
			// Codec 8 Extended is 0x8E which is 142
			tmp = make([]byte, 2)
		} else {
			return err
		}
		bytesReader.Read(tmp)

		ioElement := &IOElement{}
		ioElement.EventID = uint16(bytes2int(tmp))

		// Read total number of Elements just to increase pointer, we don't actually use that info
		bytesReader.Read(tmp)

		// read properties with size 1 byte
		bytesReader.Read(tmp)
		size := bytes2int(tmp)
		if size > 0 {
			ioElement.N1 = make([]Element, size)
			tmp1 := make([]byte, 1)
			for i := 0; i < int(size); i++ {
				element := Element{}
				element.Value = make([]byte, 1)

				bytesReader.Read(tmp)
				element.ID = uint16(bytes2int(tmp))

				bytesReader.Read(tmp1)
				copy(element.Value, tmp1)

				ioElement.N1[i] = element
			}
		}

		// read properties with size 2 bytes
		bytesReader.Read(tmp)
		size = bytes2int(tmp)
		if size > 0 {
			ioElement.N2 = make([]Element, size)
			tmp2 := make([]byte, 2)
			for i := 0; i < int(size); i++ {
				element := Element{}
				element.Value = make([]byte, 2)

				bytesReader.Read(tmp)
				element.ID = uint16(bytes2int(tmp))

				bytesReader.Read(tmp2)
				copy(element.Value, tmp2)

				ioElement.N2[i] = element
			}
		}

		// read properties with size of 4 bytes
		bytesReader.Read(tmp)
		size = bytes2int(tmp)
		if size > 0 {
			ioElement.N4 = make([]Element, size)
			tmp4 := make([]byte, 4)
			for i := 0; i < int(size); i++ {
				element := Element{}
				element.Value = make([]byte, 4)

				bytesReader.Read(tmp)
				element.ID = uint16(bytes2int(tmp))

				bytesReader.Read(tmp4)
				copy(element.Value, tmp4)

				ioElement.N4[i] = element
			}
		}

		// read properties with size 8 bytes
		bytesReader.Read(tmp)
		size = bytes2int(tmp)
		if size > 0 {
			ioElement.N8 = make([]Element, size)
			tmp8 := make([]byte, 8)
			for i := 0; i < int(size); i++ {
				element := Element{}
				element.Value = make([]byte, 8)

				bytesReader.Read(tmp)
				element.ID = uint16(bytes2int(tmp))

				bytesReader.Read(tmp8)
				copy(element.Value, tmp8)

				ioElement.N8[i] = element
			}
		}

		// read properties with size X bytes
		if avlDataPacket.CodecID == 142 {
			bytesReader.Read(tmp)
			size = bytes2int(tmp)
			if size > 0 {
				size = bytes2int(tmp)
				ioElement.NX = make([]Element, size)
				for i := 0; i < int(size); i++ {
					element := Element{}

					// Read ID
					bytesReader.Read(tmp)
					element.ID = uint16(bytes2int(tmp))

					// Read size of data
					bytesReader.Read(tmp)
					tmpX := make([]byte, bytes2int(tmp))
					element.Value = make([]byte, len(tmpX))

					bytesReader.Read(tmpX)
					copy(element.Value, tmpX)

					ioElement.NX[i] = element
				}
			}
		}

		// ioElement.Elements = elements
		avl.IOElement = *ioElement

		avlDataPacket.AVLData[index] = *avl
	}

	return err
}

func DecodeGPSElement(data []byte, avlData *AVLData) {
	avlData.GPSElement = GPSElement{}
	tmp := binary.BigEndian.Uint32(data[:4])
	lon := tmp & (0xffffffff >> 1)
	avlData.GPSElement.Longitude = float32(lon) / 10000000
	if tmp>>31 == 1 {
		avlData.GPSElement.Longitude *= -1
	}
	tmp = binary.BigEndian.Uint32(data[4:8])
	lat := tmp & (0xffffffff >> 1)
	avlData.GPSElement.Latitude = float32(lat) / 10000000
	if tmp>>31 == 1 {
		avlData.GPSElement.Latitude *= -1
	}

	avlData.GPSElement.Altitude = binary.BigEndian.Uint16(data[8:10])
	avlData.GPSElement.Angle = binary.BigEndian.Uint16(data[10:12])
	avlData.GPSElement.Satellites = uint8(data[13])
	avlData.GPSElement.Speed = binary.BigEndian.Uint16(data[13:])
}
